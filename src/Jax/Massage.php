<?php

namespace Lar\LteAdmin\Extend\LteAdminChat\Jax;

use Lar\LJS\JaxExecutor;
use Lar\LteAdmin\Extend\LteAdminChat\Models\LteChatMessage;

/**
 * Class Massage
 * @package Lar\LteAdmin\Extend\LteAdminChat\Jax
 */
class Massage extends JaxExecutor
{
    /**
     * Public method access
     *
     * @return bool
     */
    public function access() {

        return !\LteAdmin::guest();
    }

    /**
     * @param  string  $message
     */
    public function send_message(string $message)
    {
        \Lar\LteAdmin\Extend\LteAdminChat\WSEvents\Message::dispatch(LteChatMessage::create([
            'user_id' => \LteAdmin::user()->id,
            'message' => $message
        ]));
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function list()
    {
        return LteChatMessage::orderByDesc('id')->take(20)->get()->sortBy('id');
    }
}