<?php

namespace Lar\LteAdmin\Extend\LteAdminChat\Models;

use Illuminate\Database\Eloquent\Model;
use Lar\LteAdmin\Models\LteUser;

/**
 * Class LteChatMessage
 *
 * @package Lar\LteAdmin\Extend\LteAdminChat\Models
 * @property int $id
 * @property int $user_id
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Lar\LteAdmin\Models\LteUser|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\LteAdminChat\Models\LteChatMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\LteAdminChat\Models\LteChatMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\LteAdminChat\Models\LteChatMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\LteAdminChat\Models\LteChatMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\LteAdminChat\Models\LteChatMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\LteAdminChat\Models\LteChatMessage whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\LteAdminChat\Models\LteChatMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\LteAdminChat\Models\LteChatMessage whereUserId($value)
 * @mixin \Eloquent
 */
class LteChatMessage extends Model
{
    /**
     * @var string
     */
    protected $table = "lte_chat_messages";

    /**
     * @var array
     */
    protected $fillable = [
        "user_id", "message"
    ];

    /**
     * @var array
     */
    protected $with = [
        'user'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(LteUser::class, 'id', 'user_id');
    }

    /**
     * Prepare a date for array / JSON serialization.
     */
    protected function serializeDate(\DateTimeInterface $date) : string
    {
        return $date->format('Y-m-d H:i:s');
    }
}