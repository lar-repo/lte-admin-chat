<?php

namespace Lar\LteAdmin\Extend\LteAdminChat;

use Lar\LteAdmin\ExtendProvider;
use Lar\LteAdmin\Core\ConfigExtensionProvider;
use Lar\LteAdmin\Extend\LteAdminChat\Extension\Config;
use Lar\LteAdmin\Extend\LteAdminChat\Extension\Install;
use Lar\LteAdmin\Extend\LteAdminChat\Extension\Navigator;
use Lar\LteAdmin\Extend\LteAdminChat\Extension\Uninstall;
use Lar\LteAdmin\Extend\LteAdminChat\Extension\Permissions;

/**
 * Class ServiceProvider
 * @package Lar\LteAdmin\Extend\LteAdminChat
 */
class ServiceProvider extends ExtendProvider
{
    /**
     * Extension ID name
     * @var string
     */
    public static $name = "lar/lte-admin-chat";

    /**
     * Extension call slug
     * @var string
     */
    static $slug = "lar_lte_admin_chat";

    /**
     * Extension description
     * @var string
     */
    public static $description = "Chat for Lte Admin";

    /**
     * @var string
     */
    protected $navigator = Navigator::class;

    /**
     * @var string
     */
    protected $install = Install::class;

    /**
     * @var string
     */
    protected $uninstall = Uninstall::class;

    /**
     * @var string
     */
    protected $permissions = Permissions::class;

    /**
     * @var ConfigExtensionProvider|string
     */
    protected $config = Config::class;
}

