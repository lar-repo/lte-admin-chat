<?php

namespace Lar\LteAdmin\Extend\LteAdminChat\Extension;

use Lar\LteAdmin\Core\PermissionsExtensionProvider;

/**
 * Class Permissions
 * @package Lar\LteAdmin\Extend\LteAdminChat\Extension
 */
class Permissions extends PermissionsExtensionProvider {

    /**
     * Has function permission in extension
     * Fields: ["slug" => "", "description" => "", "roles" => []]
     * @return array
     */
    public function functions(): array
    {
        return [];
    }
}