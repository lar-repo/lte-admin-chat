<?php

namespace Lar\LteAdmin\Extend\LteAdminChat\Extension;

use Lar\LteAdmin\Core\InstallExtensionProvider;
use Lar\LteAdmin\Interfaces\ActionWorkExtensionInterface;

/**
 * Class Install
 * @package Lar\LteAdmin\Extend\LteAdminChat\Extension
 */
class Install extends InstallExtensionProvider implements ActionWorkExtensionInterface {

    /**
     * @return void
     */
    public function handle(): void
    {
        if (!\App::isLocal()) {
            $this->publish(__DIR__ . '/../../assets', public_path('lte-chat'));
        } else if (!is_link(public_path('lte-chat'))) {
            if (!is_dir(public_path('lte'))) mkdir(public_path('lte'), 0775, true);
            \File::link(__DIR__ . '/../../assets', public_path('lte-chat'));
        }

        $this->migrate(__DIR__ . '/../../migrations');
    }
}