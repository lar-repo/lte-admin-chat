<?php

namespace Lar\LteAdmin\Extend\LteAdminChat\Extension;

use Lar\LteAdmin\Core\NavigatorExtensionProvider;
use Lar\LteAdmin\Extend\LteAdminChat\Components\ChatButton;
use Lar\LteAdmin\Interfaces\ActionWorkExtensionInterface;

/**
 * Class Navigator
 * @package Lar\LteAdmin\Extend\LteAdminChat\Extension
 */
class Navigator extends NavigatorExtensionProvider implements ActionWorkExtensionInterface {

    /**
     * @return void
     */
    public function handle(): void
    {
        \Navigate::nav_bar_view(ChatButton::class, [], true);

        \Navigate::channel('lte-admin-chat', function () {
            return collect(\LteAdmin::user()->toArray())->merge(['roles' => \LteAdmin::user()->roles->pluck('name')]);
        }, ['guards' => ['lte']]);
    }
}