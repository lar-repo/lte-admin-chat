<?php

namespace Lar\LteAdmin\Extend\LteAdminChat\Extension;

use Lar\LteAdmin\Core\ConfigExtensionProvider;
use Lar\LteAdmin\Extend\LteAdminChat\Components\ChatList;
use Lar\LteAdmin\ExtendProvider;
use Lar\LteAdmin\LteAdmin;

/**
 * Class Config
 * @package Lar\LteAdmin\Extend\LteAdminChat\Extension
 */
class Config extends ConfigExtensionProvider {

    /**
     * @var array
     */
    protected $scripts = [
        "lte-chat/app.js"
    ];

    /**
     * @var array
     */
    protected $styles = [
        "lte-chat/app.css"
    ];

    /**
     * Config constructor.
     * @param  ExtendProvider  $provider
     */
    public function __construct(ExtendProvider $provider)
    {
        parent::__construct($provider);

        LteAdmin::$echo = true;
    }

    /**
     * On boot lte application
     */
    public function boot()
    {
        \LteAdmin::toWrapper(ChatList::class, [
            ['user' => \LteAdmin::user()]
        ], true);
    }
}