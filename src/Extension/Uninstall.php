<?php

namespace Lar\LteAdmin\Extend\LteAdminChat\Extension;

use Lar\LteAdmin\Core\UnInstallExtensionProvider;
use Lar\LteAdmin\Interfaces\ActionWorkExtensionInterface;

/**
 * Class Navigator
 * @package Lar\LteAdmin\Extend\LteAdminChat\Extension
 */
class Uninstall extends UnInstallExtensionProvider implements ActionWorkExtensionInterface {

    /**
     * @return void
     */
    public function handle(): void
    {
        if (!\App::isLocal()) {

            $this->unpublish(__DIR__ . '/../../assets', public_path('lte-chat'));
        }

        $this->migrateRollback(__DIR__ . '/../../migrations');
    }
}