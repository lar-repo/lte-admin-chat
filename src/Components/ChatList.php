<?php

namespace Lar\LteAdmin\Extend\LteAdminChat\Components;

use Lar\Tagable\Vue;

/**
 * Class ChatList
 * @package Lar\LteAdmin\Extend\LteAdminChat
 */
class ChatList extends Vue
{
    /**
     * @var string
     */
    protected $element = "chat-list";
}