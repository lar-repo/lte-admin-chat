<?php

namespace Lar\LteAdmin\Extend\LteAdminChat\Components;

use Lar\Tagable\Vue;

/**
 * Class ChatButton
 * @package Lar\LteAdmin\Extend\LteAdminChat
 */
class ChatButton extends Vue
{
    /**
     * @var string
     */
    protected $element = "chat-button";
}