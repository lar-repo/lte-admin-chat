# Lte Admin extension
## lte-admin-chat
`composer require lar/lte-admin-chat`

| Used              | Version |
|:------------------|:-------:|
| Lar WS            | ^1.0.0  |
| Lar Ljs           | ^2.1.26 |
| Lar LteAdmin      | ^2.4.10 |
