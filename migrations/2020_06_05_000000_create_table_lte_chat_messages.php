<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLteChatMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lte_chat_messages', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id');

            $table->text('message')->nullable();

            $table->timestamps();

            $table->foreign('user_id')
                ->on('lte_users')
                ->references('id')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lte_chat_messages');
    }

    /**
     * Ignore down if condition true
     *
     * @return bool
     */
    public function ignore()
    {
        return !!\DB::table('lte_settings')->count();
    }
}
